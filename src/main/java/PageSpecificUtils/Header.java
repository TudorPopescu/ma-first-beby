package PageSpecificUtils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header {

    private WebDriver driver;

    @FindBy(css = "#site-title > a")
    private WebElement title;

    @FindBy(xpath = "//a[@class='single_image_with_link']//img")
    private WebElement mainImage;

    @FindBy(css = "#site-description")
    private WebElement titleDescription;

    @FindBy(xpath = "//a[@class='wcmenucart-contents']")
    private WebElement cartButton;

    @FindBy(css = "#menu-item-563 > a")
    private WebElement ribbonBlogButton;

    @FindBy(xpath = "//*[@id=\"menu-item-568\"]/a")
    private WebElement ribbonHomeButton;

    @FindBy(xpath = "//a[contains(text(),'Connect')]")
    private WebElement ribbonConnectButton;

    public Header(WebDriver driver){
        this.driver = driver;
    }

    public boolean isTitleEqualTo(String expected){
        return title.getText().equals(expected);
    }

    public boolean isMainImageDisplayed(){
        return mainImage.isDisplayed();
    }

    public boolean isTitleDescriptionEqualTo(String expected){
        return titleDescription.getText().equals(expected);
    }

    public boolean isCartButtonDisplayed(){
        return cartButton.isDisplayed();
    }

    public boolean isRibbonBlogDisplayed(){
        return ribbonBlogButton.isDisplayed();
    }

    public boolean isRibbonHomeDisplayed(){
        return ribbonHomeButton.isDisplayed();
    }

    public boolean isRibbonConnectDisplayed(){
        return ribbonConnectButton.isDisplayed();
    }

    public Blog goToBlog(){
        ribbonBlogButton.click();
        return PageFactory.initElements(driver, Blog.class);
    }

    public Home goToHome(){
        ribbonHomeButton.click();
        return PageFactory.initElements(driver, Home.class);
    }

    public Connect goToConnect(){
        ribbonConnectButton.click();
        return PageFactory.initElements(driver, Connect.class);
    }

}
