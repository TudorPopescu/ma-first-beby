package PageSpecificUtils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Connect {

    @FindBy(className = "entry-title")
    private WebElement connectTitle;

    // Check Register section fields
    @FindBy(id = "reg_email")
    private WebElement email;
    @FindBy(id = "reg_password")
    private WebElement password;
    @FindBy(id = "registration_field_1")
    private WebElement firstName;
    @FindBy(id = "registration_field_2")
    private WebElement lastName;
    @FindBy(id = "registration_field_3")
    private WebElement phoneNumber;

    // Check Login section fields

    @FindBy(id = "username")
    private WebElement userNameOrEmail;
    @FindBy(id = "password")
    private WebElement passwordField;
    @FindBy(id = "login")
    private WebElement loginButton;
    @FindBy(id = "rememberme")
    private WebElement rememberCheck;
    @FindBy(id = "registration_field_3")
    private WebElement passLostLink;

    public boolean isTitleThere(){
        return connectTitle.isDisplayed();
    }
}
