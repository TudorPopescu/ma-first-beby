package PageSpecificUtils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Home {

    @FindBy(xpath = "//*[@id=\"menu-item-568\"]/a")
    private WebElement homeElement;

    public boolean isTitleElementThere(){
        return homeElement.isDisplayed();
    }
}
