package dictionaryoperations;

import Mainframe.Utils;

import javax.sound.midi.Soundbank;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class RandomWordDictionaryOperation implements DictionaryOperation {

    private Set<String> wordsSet;

    public RandomWordDictionaryOperation(Set<String> wordsSet) {
        this.wordsSet = wordsSet;
    }

    @Override
    public void run() throws IOException {

        System.out.println("Get a random word: ");

        String randomWord = Utils.getRandomWord(wordsSet);

        System.out.println(randomWord);
    }
}
