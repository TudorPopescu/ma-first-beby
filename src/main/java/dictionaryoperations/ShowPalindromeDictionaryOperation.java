package dictionaryoperations;

import Mainframe.Utils;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class ShowPalindromeDictionaryOperation implements DictionaryOperation {

    private Set<String> wordsSet;

    public ShowPalindromeDictionaryOperation(Set<String> wordsSet) {
        this.wordsSet = wordsSet;
    }

    @Override
    public void run() throws IOException {

        System.out.println("Show All Palindromes");

        Set<String> palindromes =
                Utils.findPalindromes(wordsSet);

        List<String> sortedPalindromes = Utils.sortSet(palindromes);

        for (String line : sortedPalindromes){
            System.out.println(line);
        }
    }
}
