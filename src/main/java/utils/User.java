package utils;

public class User {

    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;

    private User(Builder builder){


        this.firstName = builder.firstName;
        this.password = builder.password;
        this.firstName = builder.firstName;


    }

    public String getFirstName(){ return firstName;}
    public String getPassword(){ return password;}

    public static class Builder {
        private String firstName;
        private String password;

        public Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }
        public Builder withPassword(String password) {
            this.password = password;
            return this;
        }

        public User build(){
            return new User(this);
        }
    }
}
