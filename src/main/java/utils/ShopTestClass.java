package utils;

import StuffThatCouldBeUsedInTheFuture.ElementActions;
import StuffThatCouldBeUsedInTheFuture.ElementChecks;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static utils.DriverSetup.*;

public class ShopTestClass {

    public WebDriver driver;
    public WebDriverWait wait;
    public ElementChecks elementChecks;
    public ElementActions elementActions;

    static final String URL = "http://practica.wantsome.ro/shop/";

    public ShopTestClass() {
        driver = createDriver();
        elementChecks = new ElementChecks();
        elementActions = new ElementActions(driver);
    }

    @Before
    public void setUpTest(){

        setDriver(driver);
        driver.navigate().to(URL);
        wait = new WebDriverWait(driver, 10);

    }

    @After
    public void closeBrowser(){

        closeDriver();

    }
}
