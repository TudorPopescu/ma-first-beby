package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.google.common.base.Preconditions.checkNotNull;

public class DriverSetup {

    private static final InheritableThreadLocal<WebDriver> DRIVER_CONTEXT = new InheritableThreadLocal<>();

    private DriverSetup() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Returns the driver instance.
     *
     */
    public static WebDriver getDriver() {
        WebDriver driver = DRIVER_CONTEXT.get();
        checkNotNull(driver, "No driver associated with that test.");
        return driver;
    }

    /**
     * Sets the value of the driver variable from BasePage.
     *
     */
    public static void setDriver(WebDriver driver) {
        DRIVER_CONTEXT.set(driver);
    }

    /**
     * Closes the driver instance.
     */
    public static void closeDriver() {
        getDriver().quit();
        setDriver(null);
        DRIVER_CONTEXT.remove();
    }

    /**
     * The following method will be used to create the instance of WebDriver.
     * With a specific browser: firefox, Chrome (run locally)
     */
    public static WebDriver createDriver() {

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;

    }
}
