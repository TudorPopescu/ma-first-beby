package StuffThatCouldBeUsedInTheFuture;

import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Waiter extends FluentWait<WebDriver>{

    private final WebDriver driver;

    public Waiter(WebDriver driver) {
        super(driver);

        this.driver = driver;
        withTimeout(15, SECONDS);
        ignoring(NotFoundException.class);
    }

    @Override
    public FluentWait<WebDriver> withTimeout(long duration, TimeUnit unit) {
        return super.withTimeout(duration, unit);
    }
}
