package StuffThatCouldBeUsedInTheFuture;

import org.apache.commons.lang3.StringUtils;

public enum TextComparison {

    EQUALS {
        @Override
        public boolean match(String haystack, String needle) {

            return StringUtils.equals(haystack, needle);
        }
    },
    EQUALS_IGNORE_CASE {
        @Override
        public boolean match(String haystack, String needle) {

            return StringUtils.equalsIgnoreCase(haystack, needle);
        }
    },
    CONTAINS {
        @Override
        public boolean match(String haystack, String needle) {
            return StringUtils.contains(haystack, needle);
        }
    },
    CONTAINS_IGNORE_CASE {
        @Override
        public boolean match(String haystack, String needle) {
            return StringUtils.containsIgnoreCase(haystack, needle);
        }
    };

    public abstract boolean match(String haystack, String needle);
}
