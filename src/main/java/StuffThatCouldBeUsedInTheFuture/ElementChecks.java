package StuffThatCouldBeUsedInTheFuture;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;

import static utils.DriverSetup.getDriver;

public class ElementChecks {

    public boolean checkIfElementDisplayed(By locator, long durationInSeconds) {
        FluentWait<WebDriver> wait = new FluentWait<>(getDriver());
        try {
            wait
                    .withTimeout(Duration.ofSeconds(durationInSeconds))
                    .pollingEvery(Duration.ofMillis(500))
                    .ignoring(NoSuchElementException.class)
                    .until((ExpectedCondition<Boolean>) (WebDriver waitingDriver) -> getDriver()
                            .findElement(locator)
                            .isDisplayed());
            return true;
        } catch (TimeoutException timeException) {
            return false;
        }
    }

    public boolean checkIfElementDisplayed(By locator) {


        try {
            if (!getDriver().findElement(locator).isDisplayed()) {
                return false;
            }

            return true;
        } catch (NoSuchElementException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    public boolean checkIfElementDisplayed(WebElement element) {

        try {
            if (!element.isDisplayed()) {
                return false;
            }
            return true;
        } catch (NoSuchElementException exception) {
            return false;
        }
    }

    public boolean checkElementText(By locator, String compareWith, TextComparison comparison) {

        StringBuilder message = new StringBuilder();

        String text = getDriver().findElement(locator).getText();
        message.append(String.format(
                "On element %s comparing the text: \"%s\" with \"%s\" using %s. ",
                locator,
                text,
                compareWith,
                comparison));

        boolean result = comparison.match(text, compareWith);

        return result;
    }

    public boolean isCheckboxSelected(By locator) {

        boolean result = false;

        try {
            result = getDriver().findElement(locator).isSelected();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return result;
    }
}
