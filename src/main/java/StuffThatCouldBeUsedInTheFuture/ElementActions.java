package StuffThatCouldBeUsedInTheFuture;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;
import static utils.DriverSetup.getDriver;

public class ElementActions {

    protected final Wait<WebDriver> wait;
    public WebDriver driver;

    public ElementActions(WebDriver driver) {
        this.driver = driver;
        this.wait = new Waiter(this.driver).withTimeout(25, SECONDS);
    }

    public void clickOn(WebElement element) {

        try {
            element.click();
        } catch (Exception exception) {
            throw exception;
        }
    }

    public void clickOn(By locator) {
        WebElement webElement = wait.until(elementToBeClickable(locator));
        clickOn(webElement);
    }

    public void clickOn(String xpath) {
        WebElement element = getDriver().findElement(By.xpath(xpath));
        ((JavascriptExecutor) getDriver()).executeScript(
                "arguments[0].scrollIntoView({behavior: 'instant', block: 'start'});",
                element);
        wait.until(visibilityOf(element));
        clickOn(element);
    }

    public void clearAndTypeText(By locator, String content) {
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        clickOn(locator);
        clearText(locator);
        typeText(locator, content);
    }

    public void clearText(By locator) {

        try {
            getDriver().findElement(locator).clear();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void typeText(By locator, String content) {

        try {
            wait.until(elementToBeClickable(locator));
            clickOn(locator);
            getDriver().findElement(locator).sendKeys(content);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
