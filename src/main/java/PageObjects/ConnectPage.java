package PageObjects;

import org.junit.Test;
import org.openqa.selenium.By;
import utils.ShopTestClass;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class ConnectPage extends ShopTestClass {

    private static final By EMAIL_ADDRESS = By.id("reg_email");
    private static final By CONNECT_BUTTON = By.xpath("//*[@id=\"menu-item-566\"]/a");
    private static final By CONNECT_TITLE = By.xpath("//*[@id=\"content\"]/div/div/h2");

    @Test
    public void goToConnectPage(){
        elementActions.clickOn(CONNECT_BUTTON);
        elementChecks.checkIfElementDisplayed(CONNECT_TITLE);
    }
}
