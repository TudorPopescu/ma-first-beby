package PageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.ShopTestClass;

import static junit.framework.TestCase.assertTrue;

public class CalendarPage extends ShopTestClass {

    private static final By CALENDAR = By.className("react-calendar");
    private static final By PERK_ONE_TEXT = By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[2]/div/div[1]/div[2]/p");
    private static final By PERK_TWO_TEXT = By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/div[2]/p");
    private static final By PERK_THREE_TEXT = By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[2]/div/div[3]/div[2]/p");
    private static final By PERK_FOUR_TEXT = By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[2]/div/div[4]/div[2]/p");
    private static final By PERK_FIVE_TEXT = By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[2]/div/div[5]/div[2]/p");
    private static final By PERK_SIX_TEXT = By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[2]/div/div[6]/div[2]/p");
    private static final By LAST_DAY_OF_OCT = By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[1]/div/div[2]/div/div/div[2]/button[32]");
    private static final By FIRST_DAY_OF_NOVEMBER = By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[1]/div/div[2]/div/div/div[2]/button[33]");
    private static final By FIRST_DAY_OF_DECEMBER = By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[1]/div/div[2]/div/div/div[2]/button[35]");


    public CalendarPage accessCalendar(){
        driver.navigate().to("http://whitelotus.cristiancotoi.ro/");
        ((JavascriptExecutor) driver).executeScript("window.localStorage.setItem('Tudor', 'test@test.com')");
        driver.navigate().to("http://whitelotus.cristiancotoi.ro/#/calendar");

        return this;
    }

    public CalendarPage calendarDayLoads(){

        WebElement firstDayOfNov = driver.findElement(FIRST_DAY_OF_NOVEMBER);
        WebElement lastDatOfOct = driver.findElement(LAST_DAY_OF_OCT);


        lastDatOfOct.isDisplayed();
        firstDayOfNov.click();

        assertTrue("Element not found", elementChecks.checkIfElementDisplayed(PERK_ONE_TEXT, 5));
        wait.until(ExpectedConditions.visibilityOfElementLocated(PERK_ONE_TEXT));

        return this;
    }
}
