package Mainframe;

import FileReader.InputFileReader;
import FileReader.ResourceInputFileReader;
import dictionaryoperations.DictionaryOperation;
import dictionaryoperations.RandomWordDictionaryOperation;
import dictionaryoperations.SearchDictionaryOperation;
import dictionaryoperations.ShowPalindromeDictionaryOperation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        List<String> allLines = new ArrayList<>();

      //  allLines = Mainframe.Utils.readAllLinesFromResourcesFile("dex.txt");

        InputFileReader inputFileReader = new ResourceInputFileReader();
        allLines = inputFileReader.readFile("dex.txt");

        Set<String> wordsSet = Utils.removeDuplicates(allLines);

        // Using Scanner for Getting Input from User
//        Scanner in = new Scanner(System.in);
        BufferedReader in =
                new BufferedReader(new InputStreamReader(System.in));

        // keep writing things to search for words
        while(true) {

            System.out.println("Commands Menu: \n\t" +
                    "1. Search\n\t" +
                    "2. Find All Palindromes\n\t" +
                    "3. Find All Anagrams\n\t" +
                    "4. Random word\n\t" +
                    "0. Exit");

            String userMenuSelection = in.readLine();

            // Exit option
            if(userMenuSelection.equals("0")){
                System.out.println("Exiting....");
                break;
            }

            DictionaryOperation operation = null;
            switch (userMenuSelection){
                // Searching for words
                case "1":
                    operation = new SearchDictionaryOperation(wordsSet, in);
                    break;
                case "2":
                    operation = new ShowPalindromeDictionaryOperation(wordsSet);
                    break;

                // Show anagrams for input word
                case "3":
                    System.out.println("Enter a word: ");

                    Map<String, LinkedList<String>> anagrams = Utils.groupAnagrams(wordsSet);

                    for(Map.Entry<String, LinkedList<String>> entry: anagrams.entrySet())
                        if(entry.getValue().size() > 1)
                            System.out.println(entry.getKey() + "> " + entry.getValue());

                    break;

                case "4":
                    operation = new RandomWordDictionaryOperation(wordsSet);
                    break;

                default:
                    System.out.println("Please select a valid option");
            }
            if(operation != null){
                operation.run();
            }
        }
    }
}
