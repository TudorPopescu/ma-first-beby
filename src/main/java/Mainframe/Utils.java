package Mainframe;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Utils {
    /**
     * Can read a given file from the resources folder
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromResourcesFile(String fileName) throws IOException {
        List<String> result = new ArrayList<>();
        ClassLoader classLoader = Main.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }

    /**
     * Can read a given file in the working directory
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromSourceFile(String fileName) throws IOException {
        return Files.readAllLines(Paths.get(fileName));
    }

    public static Set<String> removeDuplicates(List<String> allLines) {
        Set<String> wordsSet = new HashSet<>();

        for(String line : allLines){
            wordsSet.add(line);
        }
        return wordsSet;
    }

    public static Set<String> selectWordsByLetters(Set<String> wordsSet, String word) {

        Set<String> result = new HashSet<>();

        for (String line : wordsSet) {
            if (line.contains(word))
                result.add(line);
        }
        return result;
    }

    public static Set<String> findPalindromes(Set<String> wordsList){

        Set<String> result = new HashSet<>();

        if(wordsList == null){
            System.out.println("WARNING: null parameter for findPalindrome");
            return result;
        }

        for(String line : wordsList){

            StringBuilder reversedWord = new StringBuilder(line).reverse();

            if(line.equals(reversedWord.toString()))
                result.add(line);
        }
        return result;
    }

    public static Map<String, LinkedList<String>> groupAnagrams(Collection<String> allLines) {
        Map<String, LinkedList<String>> anagramMap = new HashMap<>();
        for(String anagram : allLines) {
            char[] wordChars = anagram.toCharArray();
            Arrays.sort(wordChars);
            String sortedKey = new String(wordChars);
            LinkedList<String> anagramList = anagramMap.get(sortedKey);
            if(anagramList == null) {
                anagramMap.put(sortedKey, anagramList = new LinkedList<>());
            }
            anagramList.add(anagram);
        }
        return anagramMap;
    }

    public static List<String> sortSet(Set<String> unsorted){
        List<String> result = new ArrayList<>(unsorted);
        Collections.sort(result);
        return result;
    }

    public static String getRandomWord(Set<String> wordsSet) {
        Random randomize = new Random();
        return getRandomWordFromSet(wordsSet, randomize);
    }

    public static String getRandomWordFromSet(Set<String> wordsSet, Random random){
        List<String> wordsList = new ArrayList<>(wordsSet);
        int index = random.nextInt(wordsList.size());
        return wordsList.get(index);
    }
}
