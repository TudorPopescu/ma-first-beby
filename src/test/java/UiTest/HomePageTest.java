package UiTest;

import PageSpecificUtils.Blog;
import PageSpecificUtils.Header;
import PageSpecificUtils.Home;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import utils.*;

import static junit.framework.TestCase.assertTrue;

public class HomePageTest extends ShopTestClass {

    @Test
    public void checkHomePageTitle(){

        Header header = PageFactory.initElements(driver, Header.class);
        assertTrue(header.isTitleEqualTo("Wantsome Shop"));
        assertTrue(header.isMainImageDisplayed());
        assertTrue(header.isTitleDescriptionEqualTo("Wantsome? Come and get some!"));
        assertTrue(header.isCartButtonDisplayed());
        assertTrue(header.isRibbonBlogDisplayed());
    }

    @Test
    public void checkBlogButtonFunctionality(){
        Header header = PageFactory.initElements(driver, Header.class);
        Blog blog = header.goToBlog();
        assertTrue("IS CAPSLOCK", blog.isTitleEqualTo("BLOG"));
    }

    @Test
    public void checkHomeButtonFunctionality(){
        Header header = PageFactory.initElements(driver, Header.class);
        Blog blog = header.goToBlog();
        assertTrue("IS CAPSLOCK", blog.isTitleEqualTo("BLOG"));
        Home home = header.goToHome();
        assertTrue(home.isTitleElementThere());
    }

    @Test
    public void immutableBuilderDemo(){
        User u = new User.Builder()
                .withPassword("strong pass")
                .withFirstName("Vasile")
                .build();
    }
}
