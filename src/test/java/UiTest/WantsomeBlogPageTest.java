package UiTest;

import utils.ShopTestClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class WantsomeBlogPageTest extends ShopTestClass {

    private String baseUrl = "http://practica.wantsome.ro/blog/";

    @Test
    public void checkRibbonOptionDestination() {

        driver.navigate().to("http://practica.wantsome.ro/blog/");

        assertEquals(driver.findElement(By.linkText("Contact")).getAttribute("href"),
                baseUrl + "contact/");
        assertEquals(driver.findElement(By.linkText("Software Testing")).getAttribute("href"),
                baseUrl + "category/software-testing/");
        assertEquals(driver.findElement(By.partialLinkText(
                "basics")).getAttribute("href"),
                baseUrl + "category/testing-basics/");
        assertEquals(driver.findElement(By.partialLinkText(
                "Levels")).getAttribute("href"),
                baseUrl + "category/testing-levels/");
        assertEquals(driver.findElement(By.partialLinkText(
                "principles")).getAttribute("href"),
                baseUrl + "category/testing-principles/");
        assertEquals(driver.findElement(By.partialLinkText(
                "process")).getAttribute("href"),
                baseUrl + "category/testing-process/");
        assertEquals(driver.findElement(By.partialLinkText("Types")).getAttribute("href"),
                baseUrl + "category/testing-types/");
        assertEquals(driver.findElement(By.linkText("Register")).getAttribute("href"),
                baseUrl + "register/");
        assertEquals(driver.findElement(By.linkText("Login")).getAttribute("href"),
                baseUrl + "login/");
    }

    @Test
    public void searchBarAndCommentPostTest(){

        driver.navigate().to("http://practica.wantsome.ro/blog/");

        WebElement searchBox = driver.findElement(By.className("search-field"));
        WebElement searchButton = driver.findElement(By.className("search-submit"));
        searchBox.sendKeys("Test");
        searchButton.click();

        waitForMilliseconds(500);

        WebElement firstResult = driver.findElement(By.xpath("//*[@id=\"post-223817\"]/header/h2/a"));
        assertEquals(firstResult.getText(), "Automated Test");

        WebElement firstresultButton = driver.findElement(By.xpath("//*[@id=\"post-223817\"]/div/a"));
        assertEquals(firstresultButton.getAttribute("href"),
                "https://practica.wantsome.ro/blog/automated-test-20/");
        firstresultButton.click();

        waitForMilliseconds(500);

        List<WebElement> links = driver.findElements(By.className("meta-categories"));

        List<String> titles = new ArrayList<>();
        for (WebElement title : links) {
            titles.add(title.getText());
            System.out.println(title.getText());
        }

        WebElement postComment = driver.findElement(By.xpath("//textarea[@id='comment']"));
        WebElement postName = driver.findElement(By.xpath("//*[@id=\"author\"]"));
        WebElement postEmail = driver.findElement(By.xpath("//*[@id=\"email\"]"));
        WebElement postWebsite = driver.findElement(By.xpath("//*[@id=\"url\"]"));
        WebElement postButton = driver.findElement(By.xpath("//*[@id=\"submit\"]"));

        postComment.sendKeys("test");
        postName.sendKeys("test");
        postEmail.sendKeys("test@test.com");
        postWebsite.sendKeys("test.com");
        postButton.click();

        waitForMilliseconds(500);

        WebElement postError = driver.findElement(By.xpath("//*[@id=\"error-page\"]/p[2]"));

        assertEquals(postError.getText(), "Duplicate comment detected; it looks as though you’ve already said that!");

    }

    private void waitForMilliseconds(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
