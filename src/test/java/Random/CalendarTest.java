package Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import static utils.DriverSetup.*;

public class CalendarTest {

    public WebDriver driver;

    private static final By CALENDAR = By.className("react-calendar");
    private static final By PERK_ONE = By.className("//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[1]/div/div[2]/div/div/div[2]/button[32]");
    private static final By PERK_TWO = By.className("autofocus");
    private static final By PERK_THREE = By.className("autofocus");
    private static final By PERK_FOUR = By.className("autofocus");

    static final String URL = "http://whitelotus.cristiancotoi.ro/";

    @Before
    public void setUpTest(){

        driver = createDriver();
        setDriver(driver);
        driver.navigate().to(URL);

    }

    @After
    public void closeBrowser(){

        closeDriver();

    }

    @Test
    public void testingCalendar(){

        ((JavascriptExecutor) driver).executeScript("window.localStorage.setItem('Tudor', 'test@test.com')");
        driver.navigate().to("http://whitelotus.cristiancotoi.ro/#/calendar");


    }

    private By getCalendarButton(String day) {
        return By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[1]/div/div[2]/div/div/div[2]" + "/button[" + day + "]");
    }
}
