package DictionaryTest;

import Mainframe.Utils;
import org.junit.Test;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import static junit.framework.TestCase.assertEquals;

public class TestRandom {

    @Test
    public void testRandom_WordFinder(){
        Set<String> inputSet = new HashSet<>();
        inputSet.add("1");
        inputSet.add("2");
        inputSet.add("3");
        inputSet.add("4");
        String randomWord = Utils.getRandomWordFromSet(inputSet, new Random(1));
        assertEquals("3", randomWord);
    }

    @Test
    public void testRandom_WordWIthNoWords(){

    }
}
